import 'package:flutter/material.dart';
import 'package:voicestream/views/ReceiverView.dart';
import 'package:voicestream/views/SenderView.dart';


class SwitchView extends StatelessWidget{
  const SwitchView({super.key});


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:Center(child:
        Row(
          children: [
            ElevatedButton(onPressed: () => Navigator.push(context,
                MaterialPageRoute(builder: (_) =>  const ReceiverView())),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: const [
                    Icon(Icons.call_received),
                    SizedBox(
                      width: 4,
                    ),
                    Text(
                      "Receiver",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    )
                  ],
                )),
            const SizedBox(
              height: 24,
            ),
            const Spacer(),
            ElevatedButton(onPressed: () => Navigator.push(context,
                MaterialPageRoute(builder: (_) =>  const SenderView())),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: const [
                    Icon(Icons.send_rounded),
                    SizedBox(
                      width: 4,
                    ),
                    Text(
                      "Sender",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    )
                  ],
                )),
            const SizedBox(
              height: 24,
            ),

          ],
        ),

        ));
  }

}